<?php

namespace App\Providers;

use App\Events\UserRegisterEvent;
use App\Events\GenerateOtpCodeEvent;
use App\Listeners\UserRegisterListener;
use App\Listeners\GenerateOtpCodeListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserRegisterEvent::class => [
            UserRegisterListener::class,
        ],
        GenerateOtpCodeEvent::class => [
            GenerateOtpCodeListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
