<?php

namespace App\Listeners;

use App\Events\GenerateOtpCodeEvent;
use App\Mail\UserRegenerateOtpCodeMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class GenerateOtpCodeListener Implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\GenerateOtpCodeEvent  $event
     * @return void
     */
    public function handle(GenerateOtpCodeEvent $event)
    {
        Mail::to($event->user)->send(new UserRegenerateOtpCodeMail($event->user));
    }
}
