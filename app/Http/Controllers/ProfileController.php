<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function profile()
    {
        $data['user'] = auth()->user();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil ditampilkan',
            'data' =>  $data
        ], 200);
    }

    public function update_profile(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'photo_profile' => 'required|image|max:2048'
        ]);

        $user = Auth::user();

        if(request()->hasFile('photo_profile') && request('photo_profile') != '') {
            $file_path = storage_path().'/app/public/'.$user->photo_profile; 
            if(File::exists($file_path)) {
                File::delete($file_path);
            }
            $photo = $request->file('photo_profile')->store('photo_profile', 'public');
        } elseif($user->photo_profile) {
            $photo = $user->photo_profile;
        } else {
            $photo = null;
        }  

        $user->update([
            'name' => request('name'),
            'photo_profile' => $photo
        ]);
        
        return response()->json([
            'response_code' => "00",
            'response_message' => 'Profile berhasil diupdate',
        ], 200);
    }
}
