<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware([ 'auth', 'emailVerification', 'adminMiddleware'])->except(['index', 'show']);
    }

    public function index()
    {
        $products = Product::all();

        $data['products'] = $products;

        if (count($data['products']) <= 0) {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'data masih kosong',
            ], 200);
        } else {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'tampil data berhasil',
                'data' => $data
            ], 200);
        }
    }

    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Detail Data Product',
                'data' => $product,
            ], 200);            
        } catch (\Exception $e) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Detail Product tidak ada',
            ], 400);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $data = $request->all();
        $data['image'] = $request->file('image')->store(
            'image_products', 'public'
        );

        Product::create($data);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Tambah Produk berhasil',
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $item = Product::findOrFail($id);

        if(request()->has('image')) {
            if ($item->image) {
                $file_path = storage_path().'/app/public/'.$item->image;              
                
                if(File::exists($file_path)) {
                    File::delete($file_path);
                }

                $file = $request->file('image')->store(
                    'image_products', 'public'
                );
            }
        } elseif($item->image) {
            $file = $item->image;
        } else {
            $file = null;
        }

        $item->update([
            'category_id' => $request->category_id,
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'image' => $file,
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Update Produk berhasil',
        ], 200);
    }

    public function destroy($id)
    {
        $item = Product::findOrFail($id);

        if ($item->image) {
            $file_path = storage_path().'/app/public/'.$item->image;              
            
            if(File::exists($file_path)) {
                File::delete($file_path);
            }
        }

        $item->delete();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'berhasil Menghapus Produk',
        ], 200);
    }
}
