<?php

namespace App\Http\Controllers;

use App\Models\Order;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


class PaymentController extends Controller
{
    public function order()
    {
        $orders = Order::all();

        $data['orders'] = $orders;

        if (count($data['orders']) <= 0) {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'data masih kosong',
            ], 200);
        } else {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'tampil data berhasil',
                'data' => $data
            ], 200);
        }
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'method' => 'required',
        ]);
        
        $order = Order::create([
            'product_id' => $request->product_id,
            'method' => $request->method,
            'user_id' => auth()->user()->id,
            'order_id' =>  Str::upper(Str::random(5)),
            'method' => $request->method,
        ]);

        $order->save();

        // insert to midtrans
        $data = $this->midtrans_store($order);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Tambah Order berhasil',
            'data' => $data
        ], 200);
    }

    protected function midtrans_store(Order $order)
    {
        $server_key = base64_encode(config('app.midtrans.server_key'));
        $base_uri = config('app.midtrans.base_uri');
        $client = new Client([
            'base_uri' => $base_uri
        ]);

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Basic ' . $server_key,
            'Content-Type' => 'application/json'
        ];

        switch ($order->method) {
            case 'BCA':
                $body = [
                    'payment_type' => 'bank_transfer',
                    'transaction_details' => [
                        "order_id" => $order->order_id,
                        "gross_amount" => $order->product->price
                    ],
                    'bank_transfer' => [
                        'bank' => 'bca'
                    ]
                ];
                break;

            case 'BNI':
                $body = [
                    'payment_type' => 'bank_transfer',
                    'transaction_details' => [
                        "order_id" => $order->order_id,
                        "gross_amount" => $order->product->price
                    ],
                    'bank_transfer' => [
                        'bank' => 'bni'
                    ]
                ];
                break;

            case 'PERMATA':
                $body = [
                    'payment_type' => 'permata',
                    'transaction_details' => [
                        "order_id" => $order->order_id,
                        "gross_amount" => $order->product->price
                    ],
                ];
                break;
            
            default:
                $body = [];
                break;
        }

        $res = $client->post('/v2/charge', [
            'headers' => $headers,
            'body' => json_encode($body)
        ]);

        return json_decode($res->getBody());
    }
}
