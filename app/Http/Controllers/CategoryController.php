<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        $data['categories'] = $categories;

        if (count($data['categories']) <= 0) {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'data masih kosong',
            ], 200);
        } else {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'tampil data berhasil',
                'data' => $data
            ], 200);
        }
    }

    public function show($id)
    {
        try {
            $category = Category::findOrFail($id);

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Detail Data Category',
                'data' => $category,
            ], 200);            
        } catch (\Exception $e) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Detail Category tidak ada',
            ], 400);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $category = Category::create([
            'name' => $request->name,
        ]);

        $data['category'] = $category;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Tambah Kategori berhasil',
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $data = $request->all();

        $category = Category::findOrFail($id);

        $category->update($data);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Update Kategori berhasil',
        ], 200);
    }

    public function destroy($id)
    {
        $item = Category::findOrFail($id);
        $item->delete();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'berhasil Menghapus Kategori',
        ], 200);
    }
}
