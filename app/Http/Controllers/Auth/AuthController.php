<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Models\User;
use App\Models\OtpCode;
use Illuminate\Http\Request;
use App\Mail\UserRegisterMail;
use App\Events\UserRegisterEvent;
use App\Events\GenerateOtpCodeEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegenerateOtpCodeMail;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'name' => 'required',
            'password' => 'required|confirmed|min:8',
        ]);

        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => Hash::make($request->password)
        ]);

        $data['user'] = $user;

        // kirim email
        // Mail::to($user->email)->send(new UserRegisterMail($user));
        event(new UserRegisterEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil di register',
            'data' => $data
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Email dan Password Invalid'], 401);
        }

        $data['token'] = $token;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token
            ]
        ], 201);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Logout Berhasil'], 201);
    }

    public function update_password(Request $request)
    {
        request()->validate([
            'email' => 'email|required',
            'password' => 'required|confirmed|min:8'
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {        
            $user->update([
                'password' => Hash::make($request->password)
            ]);  

            return response()->json([
                'response_code' => "00",
                'response_message' => 'Password Berhasil Dirubah',
            ], 200);    
        } else {
            return response()->json([
                'response_code' => "01",
                'response_message' => 'Email Tidak Terdaftar',
            ], 200);
        }
    }

    public function generateOtpCode(Request $request)
    {
        $request->validate([
            'email' => 'email|required'
        ]);

        $user = User::where('email', $request->email)->first();

        $user->generate_otp_code();

        $data['user'] = $user;
        $otpSend = $user->otpcode->otp;

        // kirim kode otp
        // Mail::to($user->email)->send(new UserRegenerateOtpCodeMail($user));
        event(new GenerateOtpCodeEvent($user));

        return response()->json([
            'success' => 'true',
            'message' => 'OTP Code Berhasil di generate',
            'otp' => $user->otpcode->otp
        ], 201);
    }

    public function verificationEmail(Request $request)
    {
        $request->validate([
            'otp' => 'numeric|required'
        ]);

        $otpCode = OtpCode::where('otp', $request->otp)->first();

        if(!$otpCode) 
        {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code tidak ditemukan',
            ], 400);
        }

        $now = Carbon::now();

        if($now > $otpCode->valid_until)
        {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'otp code sudah tidak berlaku, silahkan generate ulang',
            ], 400);
        }

        // Verifiasi email, update kolom verified not NULL
        $user = User::find($otpCode->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        // menghapus otp code
        $otpCode->delete();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'email sudah terverifikasi',
        ], 400);
    }
}
