<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\UseUuid;
use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, UseUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->role_id = $model->get_role_user();
        });

        static::created(function($model) {
            $model->generate_otp_code();
        });
    }

    public function get_role_user()
    {
        $role = Role::where('name', 'user')->first();

        return $role->id;
    }

    public function isAdmin()
    {
        if($this->role) 
        {
            if($this->role->name == 'admin') 
            {
                return true;
            } 
        }
    }

    public function generate_otp_code()
    {
        do {
            $randomNumber = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $randomNumber)->first();
        } while ($check);

        $now = Carbon::now();

        // insertorUpdate 
        OtpCode::updateOrCreate(
            [ 'user_id' => $this->id ],
            [ 
                'otp' => $randomNumber, 
                'valid_until' => $now->addMinutes(5)
            ],
        );
    }

    protected $fillable = [
        'role_id',
        'name',
        'email',
        'password',
        'photo_profile'
    ];

    protected $primaryKey = 'id';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function otp()
    {
        return $this->hasOne(Otp::class);
    }

    public function otpcode()
    {
        return $this->hasOne(OtpCode::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
