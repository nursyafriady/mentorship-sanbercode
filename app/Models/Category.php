<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class Category extends Model
{
    use HasFactory, UseUuid;

    protected $guarded = [];
    protected $primaryKey = 'id';
    
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
