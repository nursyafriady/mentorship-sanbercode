<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body style="font-family: sans-serif;">
        <div style="display: block; margin: auto; max-width: 600px;" class="main">
            <h1 style="font-size: 18px; margin-top: 20px">
                Berhasil Generate Ulang OTP Code
            </h1>
            <small>Berikut adalah OTP Code anda yang baru</small>

            <div style="background: yellow; text-align: center; font-weight: bold;">
                <p>{{ $otp ?? '' }}</p>
            </div>
            <small>Batas Waktu Penggunaan 5 menit sebelum kodenya kadaluarsa</small>
        </div>
    <style>
      .main { background-color: white; }
      a:hover { border-left-width: 1em; min-height: 2em; }
    </style>
  </body>
</html>