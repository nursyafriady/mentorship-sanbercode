<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body style="font-family: sans-serif;">
        <div style="display: block; margin: auto; max-width: 600px;" class="main">
            <h1 style="font-size: 18px; margin-top: 20px">
                Selamat, Bapak/Ibu {{ $name ?? '' }} Berhasil Register
            </h1>
            <small>Silahkan gunakan kode otp dibawah, batas waktu kadaluarsa 5 menit dari sekarang</small>

            <div style="background: yellow; text-align: center; font-weight: bold;">
                <p>{{ $otp ?? '' }}</p>
            </div>
        </div>
    <style>
      .main { background-color: white; }
      a:hover { border-left-width: 1em; min-height: 2em; }
    </style>
  </body>
</html>