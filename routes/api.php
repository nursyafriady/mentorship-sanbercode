<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Auth\AuthController;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
], function ($router) {
    // Register, Login, Logout
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth');

    // Update Password
    Route::post('update-password', [AuthController::class, 'update_password'])->middleware('emailVerification');

    // Regenerate new otp code if expired
    Route::post('generate-otp-code', [AuthController::class, 'generateOtpCode']);

    // Verifikasi Email
    Route::post('verification-email', [AuthController::class, 'verificationEmail']);
});

Route::group([
    'middleware' => ['api', 'auth', 'emailVerification'],
], function ($router) {
    Route::get('get-profile', [ProfileController::class, 'profile']);
    Route::post('update-profile', [ProfileController::class, 'update_profile']);
});

Route::middleware(['api', 'auth', 'emailVerification', 'adminMiddleware'])->group(function () {
    Route::get('categories', [CategoryController::class, 'index']);
    Route::get('categories/{id}', [CategoryController::class, 'show']);
    Route::post('categories', [CategoryController::class, 'store']);
    Route::put('categories/{id}', [CategoryController::class, 'update']);
    Route::delete('categories/{id}', [CategoryController::class, 'destroy']);
});

Route::middleware('api')->group(function () {
    Route::get('product', [ProductController::class, 'index']);
    Route::get('product/{id}', [ProductController::class, 'show']);
    Route::post('product', [ProductController::class, 'store']);
    Route::put('product/{id}', [ProductController::class, 'update']);
    Route::delete('product/{id}', [ProductController::class, 'destroy']);
});

Route::middleware('api')->group(function () {
    Route::post('payment/store', [PaymentController::class, 'store'])->middleware(['auth', 'emailVerification']);
    Route::get('order', [PaymentController::class, 'order'])->middleware(['auth', 'emailVerification', 'adminMiddleware']);
});


